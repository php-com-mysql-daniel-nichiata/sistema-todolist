<?php 
require $_SERVER['DOCUMENT_ROOT'] . "/helpers/Config.php";

#Vinculando o arquivo responsavel por fazer a conexão com o banco de dados
require_once BANCO_DE_DADOS;

#Função responsável por selecionar todos as páginas do banco
function listarPaginas()
{
    
}

#Função responsável por cadastrar os dados da página no banco
function cadastrarPagina($pagina)
{
   
}

#Função responsável por selecionar a página pelo id
function buscarPagina($id)
{
    
}

#Função responsável por atualizar o registro da página
function editarPagina($pagina, $id)
{
   
}

#Função responsável por deletar o registro página
function deletarPagina($id)
{
   
}