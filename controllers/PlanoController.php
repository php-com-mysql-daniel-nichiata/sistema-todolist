<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/helpers/Config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/models/Plano.php';

/* checarLogado();
 */
function index()
{
    $planos = listarPlanos();
    return $planos;
}

function visualizar($id)
{
    $plano = buscarPlano($id);
    return $plano;
}

function cadastrar()
{

    $plano = [];

    if (!empty($_POST)) {
        $plano = [
            'titulo' => $_POST['titulo'],
            'valor' => $_POST['valor'],
            'descricao' => $_POST['descricao']
        ];

        //outra forma de cadastrar
        //$plano['titulo'] = $_POST['titulo'];
        //$plano['valor'] = $_POST['valor'];
        //$plano['descricao'] = $_POST['descricao'];

        if (cadastrarPlano($plano)) {
            header("Location:/admin/plano");
            exit;
        }
    }
    return $plano;
}

function editar($id)
{
    $plano = buscarPlano($id);

    if (!empty($_POST)) {
        $plano = [
            'titulo' => $_POST['titulo'],
            'valor' => $_POST['valor'],
            'descricao' => $_POST['descricao']
        ];
        //outra forma de editar
        //$plano['titulo'] = $_POST['titulo'];
        //$plano['valor'] = $_POST['valor'];
        //$plano['descricao'] = $_POST['descricao'];

        if (editarPlano($plano, $id)) {
            header("Location:/admin/plano");
            exit;
        }
    }


    return $plano;
}

function deletar($id)
{
    $plano = deletarPlano($id);

    if (deletarPlano($id)) {
        header("Location:/admin/plano");
        exit;
    }
}
