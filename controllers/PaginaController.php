<?php

require_once  $_SERVER['DOCUMENT_ROOT'] . "/helpers/Config.php";

#Vinculando a dependência do Model
require_once  $_SERVER['DOCUMENT_ROOT'] . "/models/Pagina.php";

#Função responsável por listar as páginas na view index.php
function index()
{
    $paginas = listarPaginas();
    return $paginas;
}

#Função responsável por visualizar os dados da páginas na view visualizar.php
function visualizar($id)
{
    $pagina = buscarPagina($id);
    return $pagina;
}

#Função responsável por cadastrar os dados das páginas na view cadastrar.php
function cadastrar()
{

    $pagina = [];

    if (!empty($_POST)) {

        $pagina['titulo'] = $_POST['titulo'];
        $pagina['slug'] = $_POST['slug'];
        $pagina['descricao'] = $_POST['descricao'];


        if (cadastrarPagina($pagina)) {

            header('Location:/admin/pagina');
            exit;
        }
    }

    return $pagina;
}

#Função responsável por editar os dados das páginas na view editar.php
function editar($id)
{

   $pagina = buscarPagina($id);

    if (!empty($_POST)) {

        $pagina['titulo'] = $_POST['titulo'];
        $pagina['slug'] = $_POST['slug'];
        $pagina['descricao'] = $_POST['descricao'];


        if (editarPagina($pagina, $id)) {

            header('Location:/admin/pagina');
            exit;
        }
    }

    return $pagina;
}

#Função responsável por deletar o registro o página
function deletar($id)
{
    if(deletarPagina(($id))) {
        header('Location:/admin/pagina');
        exit;
    }
}
