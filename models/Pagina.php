<?php

#Vinculando o arquivo responsavel por fazer a conexão com o banco de dados
require_once BANCO_DE_DADOS;

#Função responsável por selecionar todos as páginas do banco
function listarPaginas()
{
    $db = conexao();

    $sql = "SELECT * FROM paginas";

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC); 
    } catch (PDOException $e) {
        die($e->getMessage());
        return false;
    }
}




#Função responsável por cadastrar os dados da página no banco
function cadastrarPagina($pagina)
{


    $db = conexao();
    $sql = "INSERT INTO paginas (titulo, slug, descricao) 
                       VALUES (:titulo, :slug, :descricao)";
    try {
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":titulo", $pagina['titulo']);
        $stmt->bindParam(":slug", $pagina['slug']);
        $stmt->bindParam(":descricao", $pagina['descricao']);
        $stmt->execute();

        return true;
    } catch (PDOException $e) {

        die($e->getMessage());
    }
}

#Função responsável por selecionar a página pelo id
function buscarPagina($id)
{
    $db = conexao();

    $sql = "SELECT * FROM paginas WHERE id=:id";

    try {
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    } catch (\PDOException $e) {
        die($e->getMessage());
        return false;
    }
}
#Função responsável por atualizar o registro da página
function editarPagina($pagina, $id)
{

    $db = conexao();
    $sql = "UPDATE paginas

                  SET titulo=:titulo,
                      slug=:slug, 
                      descricao=:descricao 

                WHERE id=:id";
    try {
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":titulo", $pagina['titulo']);
        $stmt->bindParam(":slug", $pagina['slug']);
        $stmt->bindParam(":descricao", $pagina['descricao']);
        $stmt->bindParam(":id", $id);
        $stmt->execute();

        return true;
    } catch (PDOException $e) {
        die($e->getMessage());
        return false;
    }
}

#Função responsável por deletar o registro página
function deletarPagina($id)
{
    $db = conexao();

    $sql = "DELETE FROM paginas WHERE id=:id";
    try {
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        return true;
    } catch (PDOException $e) {
        die($e->getMessage());
        return false;
    } 
}
