<div class="col-12">
    <label for="titulo">Titulo</label>
    <input type="titulo" name="titulo" id="titulo" class="form-control" value="<?= $plano['titulo'] ?? '' ?>" placeholder="Digite o titulo" autofocus>

</div>

<div class="col-12">
    <label for="valor">Valor</label>
    <input type="valor" name='valor' id="valor" class="form-control col-sm-4 valor" value="<?= $plano['valor'] ?? '' ?>" placeholder="Digite o valor">
</div>

<div class="col-12">
    <label for="descricao">Descrição</label>
    <textarea name="descricao" id="descricao" class="form-control" rows="5"><?= $plano['descricao'] ?? '' ?></textarea>
</div>

<div class="col-12">
    <button class="btn btn-primary" type="submit">Salvar</button>
    <a class="btn btn-light" href="/admin/planos">Cancelar</a>
</div>